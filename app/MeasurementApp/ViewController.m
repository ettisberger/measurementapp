//
//  ViewController.m
//  MeasurementApp
//
//  Created by Andreas Ettisberger on 30.04.13.
//  Copyright (c) 2013 zuehlke. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    // a useless command
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
