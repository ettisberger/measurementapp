//
//  MeasurementAppTests.m
//  MeasurementAppTests
//
//  Created by Andreas Ettisberger on 30.04.13.
//  Copyright (c) 2013 zuehlke. All rights reserved.
//

#import "MeasurementAppTests.h"

@implementation MeasurementAppTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in MeasurementAppTests");
}

@end
