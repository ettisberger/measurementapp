Then /^I should see the label "(.*?)"$/ do |labelText|
    check_element_exists "label marked:'#{labelText}'"
end
